import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: Profile(
      id: 'Your Id',
      name: 'Your Name',
      des: 'Your Description',
      url:
          'https://e7.pngegg.com/pngimages/798/436/png-clipart-computer-icons-user-profile-avatar-profile-heroes-black.png',
    ));
  }
}

class Profile extends StatefulWidget {
  final String url, id, name, des;

  Profile({@required this.id, this.name, this.des, this.url});
  @override
  _ProfileState createState() {
    return _ProfileState();
  }
}

class _ProfileState extends State<Profile> with TickerProviderStateMixin {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          widget.id,
          style: TextStyle(color: Colors.black),
        ),
        backgroundColor: Colors.white,
        leading: IconButton(
          icon: Icon(Icons.add, color: Colors.black),
          onPressed: () {
            print('Clikced!!');
          },
        ),
        actions: [
          IconButton(
            icon: Icon(Icons.menu, color: Colors.black),
            onPressed: () {
              print('Clicked!!');
            },
          ),
        ],
      ),
      body: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              height: 40,
              decoration: BoxDecoration(
                border: Border.fromBorderSide(
                  BorderSide(width: 1, color: Colors.black12),
                ),
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text('See Covid-19 Business Resources',
                      style: TextStyle(color: Colors.lightBlue)),
                ],
              ),
            ),
            Container(
              padding: const EdgeInsets.all(10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Container(
                    width: 100,
                    height: 100,
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(50),
                      child: Image.network(
                        widget.url,
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                  Column(
                    children: <Widget>[
                      Text(
                        '0',
                        style: TextStyle(
                            fontSize: 20, fontWeight: FontWeight.bold),
                      ),
                      Text(
                        "Posts",
                        style: TextStyle(color: Colors.black45),
                      )
                    ],
                  ),
                  Column(
                    children: <Widget>[
                      Text('124',
                          style: TextStyle(
                              fontSize: 20, fontWeight: FontWeight.bold)),
                      Text("Followers", style: TextStyle(color: Colors.black45))
                    ],
                  ),
                  Column(
                    children: <Widget>[
                      Text('166',
                          style: TextStyle(
                              fontSize: 20, fontWeight: FontWeight.bold)),
                      Text("Following", style: TextStyle(color: Colors.black45))
                    ],
                  ),
                ],
              ),
            ),
            Container(
              padding: const EdgeInsets.all(10.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    widget.name,
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                  ),
                  Text(
                    widget.des,
                    style: TextStyle(color: Colors.grey),
                  ),
                ],
              ),
            ),
            Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  RaisedButton(
                    child: Text('Edit Profile'),
                    color: Colors.white,
                    onPressed: () {
                      _awaitReturnValueFromSecondScreen(context);
                    },
                  ),
                  // ignore: deprecated_member_use
                  RaisedButton(
                    onPressed: () {
                      print("Clicked!!");
                    },
                    child: Text('Promotions'),
                    color: Colors.white,
                  ),
                  RaisedButton(
                    onPressed: () {
                      print("Clicked!!");
                    },
                    child: Text('Insights'),
                    color: Colors.white,
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  void _awaitReturnValueFromSecondScreen(BuildContext context) async {
    final result = await Navigator.pushReplacement(
        context,
        MaterialPageRoute(
          builder: (context) => Edit(),
        ));
  }
}

class Edit extends StatefulWidget {
  Edit({Key key}) : super(key: key);
  @override
  _EditState createState() {
    return _EditState();
  }
}

class _EditState extends State<Edit> {
  TextEditingController idFieldController = new TextEditingController();
  TextEditingController nameFieldController = new TextEditingController();
  TextEditingController desFieldController = new TextEditingController();
  TextEditingController urlFieldController = new TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        leading: IconButton(
          icon: Icon(Icons.clear, color: Colors.black),
          onPressed: () {
            Navigator.pushReplacement(
                context,
                MaterialPageRoute(
                  builder: (context) => Profile(
                    url:
                        "https://thumbs.dreamstime.com/b/default-avatar-profile-icon-social-media-user-vector-default-avatar-profile-icon-social-media-user-vector-portrait-176194876.jpg",
                    id: "YourID",
                    name: "Your Name",
                    des: "Your Description",
                  ),
                ));
          },
        ),
        title: Text(
          'Edit Profile',
          style: TextStyle(color: Colors.black),
        ),
        actions: [
          IconButton(
            icon: Icon(Icons.check, color: Colors.black),
            onPressed: () {
              showAlertDialogCupertino();
            },
          ),
        ],
      ),
      body: Container(
        padding: EdgeInsets.only(
          top: 30,
          bottom: 30,
          left: 24,
          right: 24,
        ),
        child: Column(
          children: [
            TextFormField(
              controller: idFieldController,
              decoration: InputDecoration(
                labelText: 'id',
              ),
            ),
            TextFormField(
              controller: nameFieldController,
              decoration: InputDecoration(
                labelText: 'Name',
              ),
            ),
            TextFormField(
              controller: desFieldController,
              decoration: InputDecoration(
                labelText: 'Description',
              ),
            ),
            TextFormField(
              controller: urlFieldController,
              decoration: InputDecoration(
                labelText: 'Photo URL',
              ),
            ),
            Container(
                height: 70,
                child: Text(
                  "Provide your personal information, event if the accounts is used for business, a pet or something else. This won't be a part of your public profile.",
                  style: TextStyle(color: Colors.grey),
                )),
          ],
        ),
      ),
    );
  }

  showAlertDialogCupertino() {
    showDialog(
      context: context,
      builder: (_) => CupertinoAlertDialog(
        title: Text("Profile Changed"),
        content: Text('Do you want to save your edits ?'),
        actions: [
          CupertinoDialogAction(
            onPressed: () {
              Navigator.pop(context);
            },
            child: Text("No"),
          ),
          CupertinoDialogAction(
            onPressed: () {
              Navigator.pushReplacement(
                  context,
                  MaterialPageRoute(
                      builder: (context) => Profile(
                            id: idFieldController.text,
                            name: nameFieldController.text,
                            des: desFieldController.text,
                            url: urlFieldController.text,
                          )));
            },
            child: Text("Yes"),
          )
        ],
      ),
      barrierColor: Colors.black.withOpacity(0.7),
      barrierDismissible: false,
    );
  }
}
